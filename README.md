## Testing on Your Local Computer

### Step 1: Make Sure You Have Required Dependencies

- Git
- Docker
- Docker Compose

#### Example Installation on Debian-based Systems:

```
sudo apt install git docker.io docker-compose
```

### Step 2: Clone the Repository

```bash
git clone
cd vicoland-sandbox-ingress
```

### Step 3: Add Environment Variables

```bash
nano .env
```

```bash
DOMAIN=localhost
EMAIL=admin@localhost
CERT_RESOLVER=
TRAEFIK_USER=admin
TRAEFIK_PASSWORD_HASH=$2y$10$zi5n43jq9S63gBqSJwHTH.nCai2vB0SW/ABPGg2jSGmJBVRo0A.ni
```

Note that you should leave `CERT_RESOLVER` variable empty if you test your deployment locally. The password is `admin` and you might want to change it before deploying to production.

### Step 4: Set Your Own Password

If you're curious about HTTP basic auth and how it can be used with Traefik.
```bash
htpasswd -nBC 10 admin

New password:
Re-type new password:

admin:$2y$10$zi5n43jq9S63gBqSJwHTH.nCai2vB0SW/ABPGg2jSGmJBVRo0A.ni
```

The output has the following format: `username`:`password_hash`. The username doesn't have to be `admin`, feel free to change it (in the first line).

You can paste the username into the `TRAEFIK_USER` environment variable. The other part, `hashedPassword`, should be assigned to `TRAEFIK_PASSWORD_HASH`. Now you have your own `username`:`password` pair.

### Step 5: Launch Your Deployment

```bash
sudo docker-compose up -d
```

### Step 6: Test Your Deployment

```bash
curl --insecure https://localhost/
```

You can also test it in the browser:

https://localhost/

https://traefik.localhost/

## Deploying to Production



```bash
check .env file
```

### Other

Make sure you label sandbox webserver container correctly in production,


```bash
    labels:
      - traefik.http.routers.webserver.rule=Host(`${DOMAIN}`)
      - traefik.http.routers.webserver.entrypoints=https
      - traefik.http.routers.webserver.tls=true
      - traefik.http.routers.webserver.tls.certresolver=${CERT_RESOLVER}
      - traefik.http.services.webserver.loadbalancer.server.port=80  
```
Attached labels to services wile in swarm mode!